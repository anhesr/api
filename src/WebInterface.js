/**
 * Created by Anders on 17/11/2016.
 */
function WebInterface() {

    this.getVariable = function (name) {
        var win = $(window);
        var returnVal = window.frameElement.getAttribute(name);
        //console.log('getVariable: ' + name + ', val: ' + returnVal);
        //console.log(returnVal);
        return returnVal;
    };

    this.setVariable = function (key, val) {
        console.log('setVariable: ' + key + ', val: ' + val);
        window.frameElement.setAttribute(key, val);
    };
}