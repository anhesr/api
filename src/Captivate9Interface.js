/**
 * Created by Anders on 16/03/16.
 * version 1.1.1
 */

function Captivate9Interface() {

    var factor = 1;
    var ENABEL_REPORTING = false;

    // /**
    //  * Interception for point-based setting of score. Multiplies the reported score with the score weighting factor
    //  */
    // if(typeof objLMS !== 'undefined'){
    //     var _SetPointBasedScore = objLMS.SetPointBasedScore;
    //     objLMS.SetPointBasedScore = function (score, maxScore) {
    //         console.log('intercepted for: ' + score + ', ' + maxScore + ', factor: ' + factor);
    //         _SetPointBasedScore(score * factor, maxScore);
    //     };
    // }


    //START google sheets reporting
    function setupReporting() {
        if ( typeof window.parent.cpAPIEventEmitter !== 'undefined') {

            // console.log('cpAPIEventEmitter found');
            // console.log(window.parent.cpAPIEventEmitter);
            //TODO report 'course opened' to sheets
            if (!window.sessionStorage.getItem('ID')) {
                window.sessionStorage.setItem("ID", (new Date()).getTime());
                var eventData = {
                    Name: 'COURSE_OPEN',
                    Data: {slideNumber: window.parent.cpAPIInterface.getCurrentSlideIndex()},
                    timeStamp: 0
                };
                postData(createPostData(eventData));

                window.addEventListener("beforeunload", function () {
                    var eventData = {
                        Name: 'COURSE_CLOSE',
                        Data: {slideNumber: window.parent.cpAPIInterface.getCurrentSlideIndex()},
                        timeStamp: 'unknown'
                    };
                    postData(createPostData(eventData));
                });
            }

            if (!window.parent.cpAPIEventEmitter.callbackFns.CPAPI_SLIDEENTER) {
                window.parent.cpAPIEventEmitter.addEventListener("CPAPI_SLIDEENTER", function (event) {
                    console.log('Slide enter event');
                    console.log(event);
                    postData(createPostData(event));
                });
                window.parent.cpAPIEventEmitter.addEventListener("CPAPI_SLIDEEXIT", function (event) {
                    console.log('Slide exit event');
                    console.log(event);
                    postData(createPostData(event));
                });
                window.parent.cpAPIEventEmitter.addEventListener("CPAPI_QUESTIONSUBMIT", function (event) {
                    console.log('Question submit event');
                    console.log(event);
                    postData(createPostData(event));
                });
            }
        }

    }

    var createPostData = function (eventData) {
        var eventType;
        switch (eventData.Name) {
            case 'CPAPI_SLIDEENTER':
                eventType = 'SLIDE_ENTER';
                break;
            case 'CPAPI_SLIDEEXIT':
                eventType = 'SLIDE_EXIT';
                break;
            case 'CPAPI_QUESTIONSUBMIT':
                eventType = 'QUESTION_SUBMIT';
                break;
            case 'COURSE_OPEN':
                eventType = 'COURSE_OPEN';
                break;
            case 'COURSE_CLOSE':
                eventType = 'COURSE_CLOSE';
                break;
        }
        var result = 'learnerID=' + getLearnerIDImpl()
            + '&slideType=' + eventData.Data.st
            + '&timeStamp=' + new Date()
            + '&timeSinceCourseOpen=' + eventData.timeStamp
            + '&courseID=' + getVariableImpl('courseID')
            + '&sessionID=' + window.sessionStorage.getItem('ID')
            + '&eventType=' + eventType;

        if (eventType == 'QUESTION_SUBMIT') {
            result += '&questionAnsweredCorrectly=' + eventData.Data.questionAnsweredCorrectly
                + '&questionAttempts=' + eventData.Data.questionAttempts
                + '&selectedAnswer=' + eventData.Data.selectedAnswer
                + '&slideNumber=' + (Number(eventData.Data.slideNumber) + 1);
        } else {
            result += '&slideNumber=' + eventData.Data.slideNumber;
        }

        console.log('PostData: ' + result);

        return result;
    };

    var postData = function (serializedData) {
        $.ajax({
            url: 'https://script.google.com/macros/s/AKfycbyk73n4bvKnpad-mKSVAFN2abQqN7fDeD7ChriXxsYUGHApdJM7/exec',
            type: 'post',
            data: serializedData,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            success: function (response) {
                console.log('success');
                // console.log(response.data.qSeries);
                console.log(response);
            }
        });
    };
    //END google sheets reporting


    function setRawScore(score, maxScore, minScore) {
        _SetPointBasedScore(score, maxScore);
    }


    var setScoreWeighting = function (weight) {
        this.factor = weight;
    };

    var getLearnerIDImpl = function () {
        if (typeof objLMS !== 'undefined') {
            var tmp = objLMS.GetStudentID();
            console.log('getLearnerID(): ' + tmp);
            return tmp;
        } else {
            return null;
        }
    };


    this.getLearnerID = function () {
        return getLearnerIDImpl();
    };


    var getScore = function () {
        if (typeof objLMS !== 'undefined') {
            var tmp = objLMS.GetScore();
            console.log('getScore(): ' + tmp);
            return tmp;
        }

    };


    var testConnection = function () {
        console.log('connection successful');
        return true;
    };


    var getValue = function (str) {
        if (SCORM2004_CallGetValue) {
            return SCORM2004_CallGetValue(str);
        } else {
            return SCORM_CallLMSGetValue(str);
        }
    };

    this.scrubInMS = function (deltaMS) {
        var currentFrame = window.parent.cpAPIInterface.getCurrentFrame();
        var fps = window.parent.cpAPIInterface.getPlaySpeed();
        var currentMS = Math.ceil(currentFrame * 1000 / fps);
        window.parent.cpAPIInterface.navigateToTime(currentMS + deltaMS);
    };


    this.jumpToSlide = function (index) {
        console.log('jumpToSlide: ' + index);
        // cpAPIInterface.gotoSlide(index - 1);
        window.parent.cpAPIInterface.gotoSlide(index - 1);
    };

    var getVariableImpl = function (name) {
        console.log('getVariableImpl: ' + name);
        // console.log(window[name]);
        console.log(window.parent.cpAPIInterface.getVariableValue(name));
        // return window[name];
        return window.parent.cpAPIInterface.getVariableValue(name);
    };

    this.getVariable = function (name) {
        console.log('getVariable in impl: ' + name);
        return getVariableImpl(name);
    };

    this.setVariable = function (name, value) {
        console.log('setVariable: ' + name + ', value ' + value);
        // window[name] = value;
        window.parent.cpAPIInterface.setVariableValue(name, value);
    };

    this.playSlide = function () {
        console.log('playSlide()');
        window.parent.cpAPIInterface.play();
    };

    this.editScoring = function (fac, vNameArr) {
        //TODO create this method
    };

    if (typeof getVariableImpl('courseID') !== 'undefined' && ENABEL_REPORTING) {
        setupReporting();
    }
}
