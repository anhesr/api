/**
 * Created by Anders on 04/03/16.
 * version 1.1.1
 */

function StorylineInterface() {

    /**
     * Score weighting factor
     */
    var factor = 1;

    /**
     * Edit point reporting
     */
    var editPoints = false;

    /**
     * Private vars
     */
    var prevScore = 0;
    var player;
    //var variableName;
    var variableNameArr;
    var FIRST_RUN = true;
    var context = window.parent;

    /**
     * WARNING: METHOD MOST LIKELY DEPRECATED. TEST NEEDED!
     */
    this.editScoring = function (fac, vNameArr) {
        try {
            editPoints = true;
            factor = fac;
            player = context.GetPlayer();
            //variableName = vName;
            variableNameArr = vNameArr;
            //prevScore = Number(player.GetVar(variableName));

            //Untested idea to only add one eventlistener, thus only reporting once (and on 1x number of slides) (08.05.15):
            if (FIRST_RUN) {
                window.addEventListener('beforeunload', function (event) {
                    console.log('onbeforeunload');
                    ReportStatus();
                    var a = context.story.toResumeData();
                    context.objLMS.SetDataChunk(a);
                });

                FIRST_RUN = false;
                if (context.GetEntryMode() == 3) {
                    context.g_oContentResults.nScore = context.g_oContentResults.nScore * factor;
                }
            }

            //Original working reporting (08.05.15):
            // if(GetEntryMode() == 3 && FIRST_RUN){
            //    g_oContentResults.nScore = g_oContentResults.nScore * factor;
            //    FIRST_RUN = false;
            // }
            // window.addEventListener('unload', function(event) {
            //    console.log('lms.js onunload');
            //    ReportStatus();
            // });

        } catch (err) {
            console.log(err);
        }

    };

    function stopEditScore() {
        editPoints = false;
    }

// /**
//  * Interception for score setting. Multiplies the reported score with the score weighting factor (default = 1)
//  */
// var _SetScore = SetScore;
// SetScore = function(intScore, intMaxScore, intMinScore){
//     console.log('intercepted for: ' + intScore + '/' + intMaxScore + ', intMinScore: ' + intMinScore + ', factor: ' + factor);
//     _SetScore(intScore * factor, intMaxScore, intMinScore);
// };

    /**
     * Overwrite of ReportStatus() method
     * WARNING: METHOD MOST LIKELY DEPRECATED. TEST NEEDED!
     */
    ReportStatus = function () {
        console.log('Overwrite of ReportStatus()');
        if (context.g_oContentResults.strType == "quiz") {
            //Start Anders' edit
            if (editPoints) {
                try {
                    context.g_oContentResults.nScore = context.g_oContentResults.nScore * factor;
                    //make the vName variable into an array and loop through it here, adding up the 'tmp' variable
                    var tmp = 0;
                    for (var i in variableNameArr) {
                        tmp += Number(player.GetVar(variableNameArr[i]));
                    }
                    context.g_oContentResults.nScore += tmp;
                } catch (err) {
                    console.log(err);
                }

            }
            //End Anders' edit
            context.lmsAPI.SetScore(g_oContentResults.nScore, 100, 0);
        }

        context.SetStatus(normalizeStatus(g_oContentResults.strStatus));
    };

    /**
     * Interception for RecordInteraction(arrArgs) method
     */
    if (typeof context.RecordInteraction !== 'undefined') {
        var _RecordInteraction = context.RecordInteraction;
        context.RecordInteraction = function (arrArgs) {
            console.log('intercepted for RecordInteraction()');
            if (editPoints) {
                try {
                    arrArgs[8] = arrArgs[8] * factor;
                    for (var i in variableNameArr) {
                        arrArgs[8] += Number(player.GetVar(variableNameArr[i]));
                    }
                    arrArgs[8] = arrArgs[8].toString();
                } catch (err) {
                    console.log(err.stack);
                }
            }
            _RecordInteraction(arrArgs);
        };
    }


    /**
     * Overwrite of LMSUnload() method
     */
    if (typeof context.LMSUnload !== 'undefined') {
        var _LMSUnload = context.LMSUnload;
        context.LMSUnload = function () {
            console.log('Overwrite of LMSUnload()');
            if (context.g_bAPIPresent) {
                //Start Anders' edit
                if (editPoints) {
                    editPointFactor = 1;
                }
                //Start Anders' edit
                ReportStatus();

                lmsAPI.Unload();
            }
            _LMSUnload();
        };
    }


    function setRawScore(score, maxScore, minScore) {
        console.log('setRawScore(): ' + score + '/' + maxScore + ', ' + minScore);
        _SetScore(score, maxScore, minScore);
    }


    function addPoints(points) {
        console.log('addPoints(): ' + points);
        console.log(window.g_oContentResults);
        context.g_oContentResults.nScore += points;
    }


    function setScoreWeighting(weight) {
        console.log('setScoreWeighting(): ' + weight);
        this.factor = weight;
    }


    //TODO test this
    this.getLearnerID = function () {
        return context.GetStudentID();
    };


    function getScore() {
        return context.GetScore();
    }


    function testConnection() {
    }


    this.getVariable = function (name) {
        var returnVal = context.GetPlayer().GetVar(name);
        console.log('getVariable: ' + name + ', val: ' + returnVal);
        console.log(returnVal);
        return returnVal;
    };

    this.setVariable = function (key, val) {
        console.log('setVariable: ' + key + ', val: ' + val);
        context.GetPlayer().SetVar(key, val);
        // var newDataChunk = story.toResumeData();
        // objLMS.SetDataChunk(newDataChunk);
    };

    this.scrubInMS = function (ms) {
        //TODO finish this method
    };

    this.jumpToSlide = function (index) {
        console.log('jumpToSlide ' + index);
        var target = parseInt(index) + 4;
        // var target = 11;
        context.GetPlayer().showSlideIndex(target, null, true);
    };

    this.playSlide = function () {
        console.log('playSlide()');
        //TODO finish this
    };
}