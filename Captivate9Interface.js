/**
 * Created by Anders on 16/03/16.
 */
/**
 * Score weighting factor (default = 1)
 */
var factor = 1;


/**
 * Interception for point-based setting of score. Multiplies the reported score with the score weighting factor
 */
var _SetPointBasedScore = objLMS.SetPointBasedScore;
objLMS.SetPointBasedScore = function(score, maxScore){
    console.log('intercepted for: ' + score + ', ' + maxScore + ', factor: ' + factor);
    _SetPointBasedScore(score * factor, maxScore);
};

/**
 * Set raw score of user
 * @param      {Number}   score score to set
 * @param      {Number}   maxScore maximum possible score (normally 100)
 */
function setRawScore(score, maxScore){
    _SetPointBasedScore(score, maxScore);
}

/**
 * Set weighting factor
 * @param      {Number}   weight weighting factor
 */
var setScoreWeighting = function(weight){
    this.factor = weight;
};

/**
 * Get student ID
 * @param
 * @return     {Number} Student ID
 */
var getLearnerID = function(){
    var tmp = objLMS.GetStudentID();
    console.log('getLearnerID(): ' + tmp);
    return tmp;
};

/**
 * Get student score
 * @param
 * @return     {Number} Student ID
 */
var getScore = function(){
    var tmp = objLMS.GetScore();
    console.log('getScore(): ' + tmp);
    return tmp;
};

/**
 * Test to see if connection to LMS is established
 * @param
 * @return     {Boolean}
 */
var testConnection = function(){
    console.log('connection successful');
    return true;
};

/**
 * Get LMS value
 * @param      {String} key key of value to find
 * @return     {String} LMS value corresponding to the given key
 */
var getValue = function(str){
    if(SCORM2004_CallGetValue){
        return SCORM2004_CallGetValue(str);
    }else{
        return SCORM_CallLMSGetValue(str);
    }
};

/**
 * Jump to specific slide
 * @param      {Number} index index fo slide to jump to (first slide = 0)
 */
var jumpToSlide = function(index){
    cpAPIInterface.gotoSlide(index);
};
