/**
 * Created by Anders on 04/03/16.
 */
/**
 * Score weighting factor
 */
var factor = 1;

/**
 * Interception for point-based setting of score. Multiplies the reported score with the score weighting factor
 */
var _SetPointBasedScore = null;


/**
 * Set raw score of user
 * @param      {Number}   score score to set
 * @param      {Number}   maxScore maximum possible score (normally 100)
 */
function setRawScore(score, maxScore){
}

/**
 * Set weighting factor
 * @param      {Number}   weight weighting factor
 */
var setScoreWeighting = function(weight){
};

/**
 * Get student ID
 * @param
 * @return     {Number} Student ID
 */
var getLearnerID = function(){
    return null; 
};

/**
 * Get student score
 * @param
 * @return     {Number} Student ID
 */
var getScore = function(){
    return null;
};

/**
 * Test to see if connection to LMS is established
 * @param
 * @return     {Boolean}
 */
var testConnection = function(){
    console.log('connection successful');
    return true;
};

/**
 * Get LMS value
 * @param      {String} key key of value to find
 * @return     {String} LMS value corresponding to the given key
 */
var getValue = function(str){
    return null;
};

/**
 * Jump to specific slide
 * @param      {Number} index index fo slide to jump to (first slide = 0)
 */
var jumpToSlide = function(index){
};